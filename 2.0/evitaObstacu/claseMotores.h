

void parada(){
 Serial.print(" parada"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, LOW); // ponemos 135 de velocidad de giro del motor B
  delay(500); 
}

void adelante(){
//1: ADELANTE + PARADA
  Serial.print(" arranca"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, HIGH); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, HIGH); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, LOW); // ponemos 135 de velocidad de giro del motor B
  
  delay(300); // mantieneWlos motores girando durante 1000ms
  //parada();

}

void atras(){
  //2: ATR�S + PARADA
  // mueve hacia atr�s los motores A y B
  Serial.print(" atras"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, HIGH); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, HIGH); // ponemos 135 de velocidad de giro del motor B
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

void derecha(){
  //4: DERECHA + PARADA
  // motor A hacia atr�s y el B hacia delante - gira a la derecha
  Serial.print(" derecha"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel bajo por la dirA para que gire hacia atr�s
  digitalWrite (mtDerB, HIGH); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, HIGH);
  digitalWrite (mtIzqB, LOW);
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

void izquierda(){
//3: IZQUIERDA + PARADA
  // motor A hacia delante y el B hacia atr�s - gira a la izquierda
  Serial.print(" izquierda"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, HIGH); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, HIGH); // ponemos 135 de velocidad de giro del motor B
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}
