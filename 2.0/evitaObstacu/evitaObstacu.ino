
#include <Ultrasonic.h>
#include <Servo.h>

#include "claseMotores.h"

Ultrasonic ultrasonic(12,13); // (Trig PIN,Echo PIN)
Servo myservo;        //crea un objeto servo en la libreria Servo.h


//Primero asignamos los pines de dirección y velocidad para ambos motores
byte mtDerA = 11; //asignamos la dirección del motor A al pin 12 de la placa Arduin
byte mtDerB = 10;   //asignamos la dirección del motor B al pin 13 de la placa Arduino
byte mtIzqA = 7; //asignamos la velocidad del motor A al pin 10 de la placa Ardui
byte mtIzqB = 8;  //asignamos la dirección del motor B al pin 11 de la placa Arduin
int distanciaFront = 0; //variable que almacena la distancia al obstáculo
int distanciaDer = 0;
int distanciaIzq = 0;

void setup(){
   
  Serial.begin(9600);           // establecemos la librería Serie a 9600 bps
  myservo.attach(9);  //agisnamos el servo en el pin 9 
  pinMode (mtDerA, OUTPUT); // ponemos como salida el pin asignado a dirA
  pinMode (mtDerB, OUTPUT); // ponemos como salida el pin asignado a dirB
  pinMode (mtIzqA, OUTPUT); // ponemos como salida el pin asignado a speedA
  pinMode (mtIzqB, OUTPUT); // ponemos como salida el pin asignado a speedB
  myservo.write(90); //Cargamos la funcion con el valor en grados de 0 a 179º
 
}

void loop(){
  
  distanciaFront = ultrasonic.Ranging(CM);
  delay(300);
  while(distanciaFront > 30){
    adelante();
    distanciaFront = ultrasonic.Ranging(CM);
    Serial.print(distanciaFront); // CM or INC
    Serial.println(" cm Frontal" );
  }
      parada();
      myservo.write(0);  //Cargamos la funcion con el valor en grados de 0 a 179º
      delay(1000);
      distanciaIzq = ultrasonic.Ranging(CM);
      Serial.print(distanciaIzq); // CM or INC
      Serial.println(" cm Izquierda" );
      myservo.write(179);  //Cargamos la funcion con el valor en grados de 0 a 179º
      delay(1000);
      distanciaDer = ultrasonic.Ranging(CM);
      Serial.print(distanciaDer); // CM or INC
      Serial.println(" cm derecha" );
      myservo.write(90);  //Cargamos la funcion con el valor en grados de 0 a 179º
      delay(1000);
      if (distanciaDer > 30 && distanciaIzq > 30)
      derecha();
      else if (distanciaDer > 30 && distanciaIzq < 30)
      derecha();
      else if (distanciaDer < 30 && distanciaIzq > 30)
      izquierda();
      else{
      atras();
      derecha();
      derecha();
      }
         
}


