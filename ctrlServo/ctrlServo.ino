#include <Servo.h>


//--------------------------------------------------
//Declara puertos de entradas y salidas y variables
//--------------------------------------------------


Servo myservo;        //crea un objeto servo en la libreria Servo.h
 
//------------------------------------
//Funcion principal
//------------------------------------
void setup()          //Se ejecuta cada vez que el Arduino se inicia
{
  myservo.attach(9);  //agisnamos el servo en el pin 9 
}
 
//------------------------------------
//Funcion ciclicla
//------------------------------------
void loop()           //Esta funcion se mantiene ejecutando
{                     //cuando este energizado el Arduino
  myservo.write(0);   //Cargamos la funcion con el valor en grados de 0 a 179º
  delay(1000);        //Retardo entre coordenadas 
  myservo.write(90);  //Cargamos la funcion con el valor en grados de 0 a 179º
  delay(1000);        //Retardo entre coordenadas 
  myservo.write(179); //Cargamos la funcion con el valor en grados de 0 a 179º
  delay(1000);        //Retardo entre coordenadas
  myservo.write(90);  //Cargamos la funcion con el valor en grados de 0 a 179º
  delay(1000);        //Retardo entre coordenadas
  
}
 
//Fin programa EJEMLO1
 
