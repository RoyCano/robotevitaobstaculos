
#include <Ultrasonic.h>//carga de la librería para el HCSR04
#include <Servo.h>//Carga librería para control del servo

Ultrasonic ultrasonic(12,13); // (Trig PIN,Echo PIN)
Servo myservo;        //crea un objeto servo en la libreria Servo.h


//Primero asignamos los pines de dirección y velocidad para ambos motores
byte mtDerA = 11; //asignamos la dirección del motor A al pin 12 de la placa Arduin
byte mtDerB = 10;   //asignamos la dirección del motor B al pin 13 de la placa Arduino
byte mtIzqA = 7; //asignamos la velocidad del motor A al pin 10 de la placa Ardui
byte mtIzqB = 8;  //asignamos la dirección del motor B al pin 11 de la placa Arduin
int distanciaFront = 0; //variable que almacena la distancia al obstáculo
int distanciaDer = 0;
int distanciaIzq = 0;

void setup(){
   
  Serial.begin(9600); // establecemos la comunicación Serie a 9600 bps
  myservo.attach(9);  //agisnamos el servo en el pin 9 
  pinMode (mtDerA, OUTPUT); 
  pinMode (mtDerB, OUTPUT); 
  pinMode (mtIzqA, OUTPUT); 
  pinMode (mtIzqB, OUTPUT); 
  myservo.write(90); //Cargamos la funcion con el valor en grados de 0 a 179
 
}

void loop(){
  
  distanciaFront = ultrasonic.Ranging(CM); //lectura inicial de la distancia
  delay(300);
  while(distanciaFront > 30){
    adelante();
    distanciaFront = ultrasonic.Ranging(CM); //lectura permanente de la distancia 
    Serial.print(distanciaFront); // Mostramos lectura por el monitor serial
    Serial.println(" cm Frontal" );
  }
      parada();
      myservo.write(0);  //Girámos el servo a la derecha para detectar obstáculo
      delay(1000);
      distanciaIzq = ultrasonic.Ranging(CM);
      Serial.print(distanciaIzq); // CM or INC
      Serial.println(" cm Izquierda" );
      myservo.write(179);  //Girámos el servo a la izquierda para detectar obstáculo
      delay(1000);
      distanciaDer = ultrasonic.Ranging(CM);
      Serial.print(distanciaDer); // CM or INC
      Serial.println(" cm derecha" );
      myservo.write(90);  
      delay(1000);
      if (distanciaDer > 30 && distanciaIzq > 30) //utilizamos condicionales para tomar decisión hacia donde girar
      derecha(); //llamamos función para giro a la derecha
      else if (distanciaDer > 30 && distanciaIzq < 30)
      derecha();
      else if (distanciaDer < 30 && distanciaIzq > 30)
      izquierda();
      else{
      atras();
      derecha();
      derecha();
      }
         
}

void parada(){ //Función para detener
 Serial.print(" parada"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, LOW); // ponemos 135 de velocidad de giro del motor B
  delay(500); 
}

void adelante(){
//1: ADELANTE + PARADA
  Serial.print(" arranca"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, HIGH); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, HIGH); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, LOW); // ponemos 135 de velocidad de giro del motor B
  
  delay(300); // mantieneWlos motores girando durante 1000ms
  //parada();

}

void atras(){
  //2: ATRÁS + PARADA
  // mueve hacia atrás los motores A y B
  Serial.print(" atras"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, HIGH); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, HIGH); // ponemos 135 de velocidad de giro del motor B
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

void derecha(){
  //4: DERECHA + PARADA
  // motor A hacia atrás y el B hacia delante - gira a la derecha
  Serial.print(" derecha"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel bajo por la dirA para que gire hacia atrás
  digitalWrite (mtDerB, HIGH); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, HIGH);
  digitalWrite (mtIzqB, LOW);
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

void izquierda(){
//3: IZQUIERDA + PARADA
  // motor A hacia delante y el B hacia atrás - gira a la izquierda
  Serial.print(" izquierda"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, HIGH); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, HIGH); // ponemos 135 de velocidad de giro del motor B
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

