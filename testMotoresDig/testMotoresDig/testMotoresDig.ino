//Primero asignamos los pines de dirección y velocidad para ambos motores
int mtDerA = 11; //asignamos la dirección del motor A al pin 12 de la placa Arduin
int mtDerB = 10;   //asignamos la dirección del motor B al pin 13 de la placa Arduino
int mtIzqA = 8; //asignamos la velocidad del motor A al pin 10 de la placa Ardui
int mtIzqB = 9;  //asignamos la dirección del motor B al pin 11 de la placa Arduin

void parada(){
 Serial.print(" parada"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, LOW); // ponemos 135 de velocidad de giro del motor B
  delay(500); 
}


void setup(){
   
  Serial.begin(9600);           // establecemos la librería Serie a 9600 bps
  Serial.println("¡Test de Movimiento para Motores DC!");   // mostramos en pantalla
  pinMode (mtDerA, OUTPUT); // ponemos como salida el pin asignado a dirA
  pinMode (mtDerB, OUTPUT); // ponemos como salida el pin asignado a dirB
  pinMode (mtIzqA, OUTPUT); // ponemos como salida el pin asignado a speedA
  pinMode (mtIzqB, OUTPUT); // ponemos como salida el pin asignado a speedB
 }

void loop()
{
  adelante();
  atras();
  derecha();
  izquierda();
  
}

void adelante(){
//1: ADELANTE + PARADA
  Serial.print(" arranca"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, HIGH); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, HIGH); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, LOW); // ponemos 135 de velocidad de giro del motor B
  
  delay(1000); // mantieneWlos motores girando durante 1000ms
  parada();

}

void atras(){
  //2: ATRÁS + PARADA
  // mueve hacia atrás los motores A y B
  Serial.print(" atras"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, HIGH); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, HIGH); // ponemos 135 de velocidad de giro del motor B
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

void derecha(){
  //4: DERECHA + PARADA
  // motor A hacia atrás y el B hacia delante - gira a la derecha
  Serial.print(" derecha"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, LOW); // sacamos nivel bajo por la dirA para que gire hacia atrás
  digitalWrite (mtDerB, HIGH); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, HIGH);
  digitalWrite (mtIzqB, LOW);
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

void izquierda(){
//3: IZQUIERDA + PARADA
  // motor A hacia delante y el B hacia atrás - gira a la izquierda
  Serial.print(" izquierda"); // mostramos en pantalla la frase entrecomillada
  digitalWrite (mtDerA, HIGH); // sacamos nivel alto por la dirA para que gire adelante
  digitalWrite (mtDerB, LOW); // sacamos nivel alto por la dirB para que gire adelante
  digitalWrite (mtIzqA, LOW); // ponemos 135 de velocidad de giro del motor A
  digitalWrite (mtIzqB, HIGH); // ponemos 135 de velocidad de giro del motor B
  delay(1000); // mantiene los motores girando durante 1000ms
  parada();

}

